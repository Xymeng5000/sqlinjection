package sqlinjection.sqliexample.sqlinjection0717;



import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String TB_NAME = "usertable";
    public static final String ID = "_id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TB_NAME + " (" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + USERNAME + " VARCHAR," + PASSWORD + " VARCHAR )");
        db.execSQL("INSERT INTO " + TB_NAME + "(" + USERNAME + "," + PASSWORD + ") VALUES" + "('admin','admin888')");
        db.execSQL("INSERT INTO " + TB_NAME + "(" + USERNAME + "," + PASSWORD + ") VALUES" + "('root','root123')");
        db.execSQL("INSERT INTO " + TB_NAME + "(" + USERNAME + "," + PASSWORD + ") VALUES" + "('wanqing','wanqing')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        super.onOpen(db);
    }
}